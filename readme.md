# Moxuo Libs

Libs for making game in c++
or graphical app

- mxmaths: small c/c++ maths header lib.
- mxrender: small c++ graphical static lib. (.h and .cpp to include)
- ?

The libs require glfw for now.
(+ glad and stb_image that are included by default)

## MXR TODOS

- [x] Remake the folder architecture (need to rework cmake files, im bad at writting them)
- [x] Basic Context
- [x] Shaders
- [x] 2D Textures
- [x] Buffer layout
- [ ] Vertex buffers wrappers (static and dynamic)
- [x] batching vertex (simple, colored, textured)
- [x] Camera (orthographic)
- [x] Camera controller
- [x] tilemap renderer
- [x] tileset
- [x] tile rule (needs work)
- [x] Simple events (events type and manager)
- [ ] text rendering
- [ ] simple gui
- [ ] more...
