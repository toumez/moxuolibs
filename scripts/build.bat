@echo off

cd ../
mkdir build
cd build
mkdir mingw_win
cd mingw_win
echo Building example [testbed] into build/mingw_win/
cmake ../../examples/testbed/ -G "MinGW Makefiles"
mingw32-make .
