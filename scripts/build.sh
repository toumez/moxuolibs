#!/bin/bash

cd ../
mkdir build
cd build
mkdir x11
cd x11
echo Building example [testbed] into build/x11/
cmake ../../examples/testbed/ -G "Unix Makefiles"
make .
