#version 410
layout (location = 0) in vec3  a_Pos;
layout (location = 1) in vec4  a_Color;
layout (location = 2) in vec2  a_TexCoords;
layout (location = 3) in float a_TexID;

uniform mat4 u_view;
uniform mat4 u_proj;
uniform mat4 u_model;

out vec4  v_Color;
out vec2  v_texCoords;
out float v_texID;

void main() {
    mat4 mvp = u_proj * u_view * u_model;

    v_Color     = a_Color;
    v_texCoords = a_TexCoords;
    v_texID     = a_TexID;

    gl_Position = mvp * vec4(a_Pos, 1);
}
