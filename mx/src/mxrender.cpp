#include <mxrender/mxrender.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>


// DEBUG ASSERT
#ifndef MXR_ASSERT
    #include <assert.h>
    #define MXR_ASSERT(x) assert(x)
#endif //MXM_ASSERT

// LOG
#ifndef MXRLOG
    #include <stdio.h>
    #define MXRLOG_(x)      { printf("%s:%s> %s", __FILE__, __LINE__, x); }
    #define MXRLOG(x, ...) { printf("%s:%s> ", __FILE__, __LINE__); printf(x, __VA_ARGS__); }
#endif

// ERRORS
#ifndef MXRERROR
    #include <stdio.h>
    #define MXRERROR_(f) { printf("ERROR:"); printf(f); }
    #define MXRERROR(f, ...) { printf("ERROR:"); printf(f, __VA_ARGS__); }
#endif

// time
// static mx::time_infos_t mx::time_infos;

#include <bitset>

// FILE HANDLING
#include <sstream>
#include <fstream>
#include <cstdarg>


typedef struct 
{
    std::string* dirs;
    std::string  end;
} __mxr_path;
#if defined(__unix__)
    #define MXR_MAX_PATH 1024
    #include <unistd.h>
    MXRDEF std::string mx::__mxr_cwd()
    {
        char buffer[MXR_MAX_PATH];
        if(getcwd(buffer, sizeof(buffer)) != NULL)
        {
            std::string abspath(buffer);
            std::string::size_type pos = abspath.find_last_of("\\/");
            return abspath.substr(0, pos);
        }
        return std::string("");
    }

    MXRDEF std::string mx::__mxr_absolute_path(std::string p)
    {
        std::string cwd = mx::__mxr_cwd();
        return cwd + "/" + p;
    }
#elif defined(_WIN32) || defined(_WIN64)
    #define MXR_MAX_PATH 1024
    #include <windows.h>
    MXRDEF std::string mx::__mxr_cwd()
    {
        char buffer[MXR_MAX_PATH];
        GetModuleFileName( NULL, buffer, MXR_MAX_PATH );
        std::string::size_type position = std::string( buffer ).find_last_of( "\\/" );
        return std::string( buffer ).substr( 0, position);
    }

    MXRDEF std::string mx::__mxr_absolute_path(std::string p)
    {
        std::string cwd = mx::__mxr_cwd();
        return cwd + "/" + p;
    }
#endif
MXRDEF std::string mx::___read_file(std::string path)
{
    std::string abs_path = mx::__mxr_absolute_path(path);
    std::ifstream f;
    std::string data = "";
    f.exceptions(std::ifstream::badbit | std::ifstream::failbit);
    try {
        f.open(abs_path, std::ios::in);

        std::stringstream ss;
        ss << f.rdbuf();
        data = ss.str();
    } catch(std::ifstream::failure e)
    {
        MXRERROR("FileReadingError[at %s]: (%d) %s\n", abs_path.c_str(), e.code().value(), e.what());
    }
    return data;
}

////////////////////////////////////////////
//                 TEXTURE                //
////////////////////////////////////////////

MXRDEF mx::texture_t mx::texture::create(unsigned char* data, int w, int h, int channels)
{
    mx::texture_t t;
    t.width = w;
    t.height = h;
    #ifdef MXRENDER_GL
    glGenTextures(1, &t.rendererID);
    glBindTexture(GL_TEXTURE_2D, t.rendererID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, (channels == 3)? GL_RGB : GL_RGBA, w, h, 0, (channels == 3)? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0); // unbind
    #endif
    // printf("%d/%d (c: %d) => %d\n", w, h, channels, t.rendererID);
    return t;
}

MXRDEF void mx::texture::bind(mx::texture_t t, int texture_i)
{
    glActiveTexture(GL_TEXTURE0 + texture_i);
    t.binding_index = texture_i;
    glBindTexture(GL_TEXTURE_2D, t.rendererID);
    // glActiveTexture(GL_TEXTURE0);
}

MXRDEF void mx::texture::unbind(mx::texture_t t)
{
    glActiveTexture(GL_TEXTURE0 + t.binding_index);
    glBindTexture(GL_TEXTURE_2D, 0);
    // glActiveTexture(GL_TEXTURE0);
}

MXRDEF mx::texture_t mx::texture::load_from_file(const char* path) 
{
    int width, height, channels;
    std::string abspath = __mxr_absolute_path(path);
    unsigned char* data = stbi_load(abspath.c_str(), &width, &height, &channels, 0);
    if(!data)
    {
        MXRERROR("STBI_ImageReadingError: Failed to load the image data at [%s].\n", abspath.c_str());
    }
    mx::texture_t t = mx::texture::create(data, width, height, channels);
    return t;
}

////////////////////////////////////////////
//                 SHADER                 //
////////////////////////////////////////////

MXRDEF mx::shader_t mx::shader::create(const char* vcode, const char* fcode)
{
    shader_t shader;
    #ifdef MXRENDER_GL
    shader.rendererID = glCreateProgram();
    uint32_t vid = load_shader(vcode, GL_VERTEX_SHADER);
    uint32_t fid = load_shader(fcode, GL_FRAGMENT_SHADER);
    glAttachShader(shader.rendererID, vid);
    glAttachShader(shader.rendererID, fid);
    glLinkProgram(shader.rendererID);
    int success; char log[512];
    glGetProgramiv(shader.rendererID, GL_LINK_STATUS, &success);
    if(!success)
    {
        glGetProgramInfoLog(shader.rendererID, 512, NULL, log);
        MXRERROR("ShaderLinkError: %s", log);
    }
    #endif
    return shader;
}

MXRDEF void mx::shader::begin(mx::shader_t shader)
{
    #ifdef MXRENDER_GL
    glUseProgram(shader.rendererID);
    #endif
}

MXRDEF void mx::shader::end(mx::shader_t shader)
{
    #ifdef MXRENDER_GL
    glUseProgram(0);
    #endif
}

// UNIFORMS
template<>
void mx::shader::set_uniform(mx::shader_t s, std::string name, float value) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform1f(loc, value);
    #endif
}

template<>
void mx::shader::set_uniform(shader_t s, std::string name, double value) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform1f(loc, (float)value);
    #endif
}

template<>
void mx::shader::set_uniform(shader_t s, std::string name, int value) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform1i(loc, value);
    #endif
}

template<>
void mx::shader::set_uniform(shader_t s, std::string name, bool value) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform1i(loc, (int)value);
    #endif
}

template<>
void mx::shader::set_uniform(mx::shader_t s, std::string name, vec3 value) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform3f(loc, value.x, value.y, value.z);
    #endif
}

template<>
void mx::shader::set_uniform(mx::shader_t s, std::string name, vec4 value) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform4f(loc, value.x, value.y, value.z, value.w);
    #endif
}

MXRDEF void mx::shader::set_uniform_mat(mx::shader_t s, std::string name, float* valueptr, int size) {
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    switch(size)
    {
        case 3:
            glUniformMatrix3fv(loc, 1, GL_FALSE, valueptr);
            break;
        case 4:
            glUniformMatrix4fv(loc, 1, GL_FALSE, valueptr);
            break;
    }
    #endif
}


template<>
void mx::shader::set_uniform_array(mx::shader_t s, std::string name, int* arr, uint32_t count)
{
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform1iv(loc, count, arr);
    #endif
}

template<>
void mx::shader::set_uniform_array(mx::shader_t s, std::string name, float* arr, uint32_t count)
{
    #ifdef MXRENDER_GL
    uint32_t loc = glGetUniformLocation(s.rendererID, name.c_str());
    glUniform1fv(loc, count, arr);
    #endif
}

// LOADERS
MXRDEF mx::shader_t mx::shader::load_from_files(std::string vpath, std::string fpath) {
    // read each files
    ::std::string vcode = ___read_file(vpath);
    ::std::string fcode = ___read_file(fpath);

    return mx::shader::create(vcode.c_str(), fcode.c_str());
}


MXRDEF uint32_t mx::shader::load_shader(std::string code, int type)
{
    const char* str = code.c_str();
    #ifdef MXRENDER_GL
    char infoLog[512];

    uint32_t s_id = glCreateShader(type);
    glShaderSource(s_id, 1, &str, NULL);
    glCompileShader(s_id);
    int compiled = 0;
    glGetShaderiv(s_id, GL_COMPILE_STATUS, &compiled);
    if(!compiled)
    {
        glGetShaderInfoLog(s_id, 512, NULL, infoLog);
        MXRERROR("ShaderCompileError: %s\n", infoLog);
    }
    #endif
    return s_id;
}

////////////////////////////////////////////
//                 EVENTS                 //
////////////////////////////////////////////

MXRDEF void mx::event_manager::register_on(mx::event_manager_t* manager, std::string event_name, void* obj, void* eventfnc)
{
    manager->registered_events[event_name].push_back(
        {
            eventfnc,
            obj
        }
    );
}


// window resized
MXRDEF void mx::event_manager::register_on_resize        (mx::event_manager_t* manager, void* obj, mx::events::resize fnc)
{
    manager->registered_events["resize"].push_back(
        {
            (void*)fnc,
            obj
        }
    );
}
MXRDEF void mx::event_manager::invoke_resize(mx::event_manager_t* manager, int w, int h)
{
    for(const auto& fnc : manager->registered_events["resize"])
    {
        mx::events::resize f = (mx::events::resize)fnc.event_fnc;
        f(fnc.obj, w, h);
    }
}

// key pressed
MXRDEF void mx::event_manager::register_on_key_pressed   (mx::event_manager_t* manager, void* obj, mx::events::key_pressed fnc)
{
    manager->registered_events["key_pressed"].push_back(
        {
            (void*)fnc,
            obj
        }
    );
}
MXRDEF void mx::event_manager::invoke_key_pressed(mx::event_manager_t* manager, int key, int scancode, int mods)
{
    for(const auto& fnc : manager->registered_events["key_pressed"])
    {
        mx::events::key_pressed f = (mx::events::key_pressed)fnc.event_fnc;
        f(fnc.obj, key, scancode, mods);
    }
}

// key down
MXRDEF void mx::event_manager::register_on_key_down   (mx::event_manager_t* manager, void* obj, mx::events::key_down fnc)
{
    manager->registered_events["key_down"].push_back(
        {
            (void*)fnc,
            obj
        }
    );
}
MXRDEF void mx::event_manager::invoke_key_down(mx::event_manager_t* manager, int key, int scancode, int mods)
{
    for(const auto& fnc : manager->registered_events["key_down"])
    {
        mx::events::key_down f = (mx::events::key_down)fnc.event_fnc;
        f(fnc.obj, key, scancode, mods);
    }
}

// key released
MXRDEF void mx::event_manager::register_on_key_released  (mx::event_manager_t* manager, void* obj, mx::events::key_released fnc)
{
     manager->registered_events["key_released"].push_back(
        {
            (void*)fnc,
            obj
        }
    );
}
MXRDEF void mx::event_manager::invoke_key_released(mx::event_manager_t* manager, int key, int scancode, int mods)
{
    for(const auto& fnc : manager->registered_events["key_released"])
    {
        mx::events::key_released f = (mx::events::key_released)fnc.event_fnc;
        f(fnc.obj, key, scancode, mods);
    }
}

// mouse scrolled
MXRDEF void mx::event_manager::register_on_scroll        (mx::event_manager_t* manager, void* obj, mx::events::scroll fnc)
{ 
    manager->registered_events["scroll"].push_back(
        {
            (void*)fnc,
            obj
        }
    );
}
MXRDEF void mx::event_manager::invoke_scroll(mx::event_manager_t* manager, float xoff, float yoff)
{
    for(const auto& fnc : manager->registered_events["scroll"])
    {
        mx::events::scroll f = (mx::events::scroll)fnc.event_fnc;
        f(fnc.obj, xoff, yoff);
    }
}

// mouse button pressed
MXRDEF void mx::event_manager::register_on_mouse_pressed (mx::event_manager_t* manager, void* obj, mx::events::mouse_pressed fnc)
{
     manager->registered_events["mouse_pressed"].push_back(
        {
            (void*)fnc,
            obj
        }
    );
}
MXRDEF void mx::event_manager::invoke_mouse_pressed(mx::event_manager_t* manager, int button, int mods)
{
    for(const auto& fnc : manager->registered_events["mouse_pressed"])
    {
        mx::events::mouse_pressed f = (mx::events::mouse_pressed)fnc.event_fnc;
        f(fnc.obj, button, mods);
    }
}

// mouse button released
MXRDEF void mx::event_manager::register_on_mouse_released(mx::event_manager_t* manager, void* obj, mx::events::mouse_released fnc)
{
     manager->registered_events["mouse_released"].push_back(
        {
            (void*)fnc,
            obj
        }
    );
}
MXRDEF void mx::event_manager::invoke_mouse_released(mx::event_manager_t* manager, int button, int mods)
{
    for(const auto& fnc : manager->registered_events["mouse_released"])
    {
        mx::events::mouse_released f = (mx::events::mouse_released)fnc.event_fnc;
        f(fnc.obj, button, mods);
    }
}

// MOUSE MOVE
MXRDEF void mx::event_manager::register_on_mouse_move(mx::event_manager_t* manager, void* obj, mx::events::mouse_move fnc)
{
     manager->registered_events["mouse_move"].push_back(
        {
            (void*)fnc,
            obj
        }
    );
}
MXRDEF void mx::event_manager::invoke_mouse_move(mx::event_manager_t* manager, float xpos, float ypos)
{
    for(const mx::event_manager_t::__event_descriptor_t& fnc : manager->registered_events["mouse_move"])
    {
        mx::events::mouse_move f = (mx::events::mouse_move)fnc.event_fnc;
        f(fnc.obj, xpos, ypos);
    }
}

////////////////////////////////////////////
//                 CAMERA                //
////////////////////////////////////////////

static void camdefault_resizecallback(void* obj, int w, int h)
{
    mx::camera_t* cam = (mx::camera_t*)obj;
    // Recalculate proj
    float aspect_ratio = float(w)/float(h);
    cam->size = { w * .5f, h * .5f };
    float z2_inv = .5f / cam->zoom;
    cam->proj = mx_ortho_proj(
        -cam->size.x * z2_inv, 
         cam->size.x * z2_inv, 
        -cam->size.y * z2_inv, 
         cam->size.y * z2_inv
    );
} 

static void camdefault_keycallback(void* obj, int key, int scancode, int mods)
{
    mx::camera_t* cam = (mx::camera_t*)obj;
    float s = cam->controller.move_speed * (float)mx::time_infos.delta_time * (1.f / cam->zoom);
    switch(key)
    {
        case GLFW_KEY_W: mx::camera::move(cam, { 0.f,    s, 0.f}); break; // down
        case GLFW_KEY_S: mx::camera::move(cam, { 0.f,   -s, 0.f}); break; // up
        case GLFW_KEY_D: mx::camera::move(cam, {   s,  0.f, 0.f}); break; // left
        case GLFW_KEY_A: mx::camera::move(cam, {  -s,  0.f, 0.f}); break; // right
    }
} 

static void camdefault_mousescrollcallback(void* obj, float xoff, float yoff)
{
    mx::camera_t* cam = (mx::camera_t*)obj;
    float zoom_change = yoff * cam->controller.zoom_speed * (float)mx::time_infos.delta_time;
    cam->zoom += (float)zoom_change;
    // if(cam->zoom > (1.f / cam->controller.min_zoom)) cam->zoom = 1.f / cam->controller.min_zoom;
    // if(cam->zoom < (1.f / cam->controller.max_zoom)) cam->zoom = 1.f / cam->controller.max_zoom;

    float z2_inv = .5f / cam->zoom;
    cam->proj = mx_ortho_proj(
        -cam->size.x * z2_inv, 
         cam->size.x * z2_inv, 
        -cam->size.y * z2_inv, 
         cam->size.y * z2_inv
    );
 } 

MXRDEF mx::camera_t mx::camera::create(vec3 pos, vec2 size, float zoom)
{
    mx::camera_t cam;
    cam.zoom = zoom;
    float z2_inv = .5f / zoom;
    cam.proj = mx_ortho_proj(-size.x * z2_inv, size.x * z2_inv, size.y * z2_inv, -size.y * z2_inv, -1, 1);
    cam.pos  = pos;
    cam.view = mx_mat4_translate(mx_mat4_identity(), pos);
    cam.size = { size.x, size.y };

    cam.controller.resize_callback      = camdefault_resizecallback;
    cam.controller.keypress_callback    = camdefault_keycallback;
    cam.controller.mousescroll_callback = camdefault_mousescrollcallback;
    return cam;
}

MXRDEF void mx::camera::move(mx::camera_t* cam, vec3 pos)
{
    #if defined(__mxmaths_h__)
    cam->pos = mx_vec3_add(cam->pos, pos);
    cam->view = mx_mat4_translate(mx_mat4_identity(), cam->pos);
    #endif
}

MXRDEF void mx::camera::set_uniforms(mx::camera_t cam, mx::shader_t s)
{
    mx::shader::begin(s);
    #if defined(__mxmaths_h__)
    mx_mat4_valueptr projptr = mx_mat4_raw(cam.proj);
    mx::shader::set_uniform_mat(s, "u_proj", projptr.ptr, 4);

    mx_mat4_valueptr viewptr = mx_mat4_raw(cam.view);
    mx::shader::set_uniform_mat(s, "u_view", viewptr.ptr, 4);
    #endif
    mx::shader::end(s);
}



////////////////////////////////////////////
//                 CONTEXT                //
////////////////////////////////////////////

MXRDEF mx::context_t mx::context::create(int w, int h, std::string name, bool can_camera_move)
{
    mx::context_t ctx;
    ctx.width = w;
    ctx.height = h;
    ctx.name = name;
    ctx.can_camera_move = can_camera_move;

    // init glfw
    int success = glfwInit();
    if(!success)
    {
        MXRERROR_("GLFW FAILED TO INITIALIZE\n");
        exit(1);
    }

    glfwWindowHint(GLFW_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_VERSION_MAJOR, 3);
    ctx.window_handle = glfwCreateWindow(w, h, name.c_str(), NULL, NULL);
    glfwMakeContextCurrent(ctx.window_handle);

    glfwSwapInterval(0);

    // init api (GL/Vulkan/Metal/DX...)
    #if defined(MXRENDER_USE_GL)
    success = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
    #elif defined(MXRENDER_USE_GLES)
    success = gladLoadGLES2Loader((GLADloadproc)glfwGetProcAddress)
    #endif
    if(!success)
    {
        MXRERROR_("Render api couldn't be loaded\n");
        exit(2);
    }

    #ifdef MXRENDER_GL
        glViewport(0, 0, w, h);
    #endif

    
    mx::time_infos.delta_time = 1. / 60.; // 60 FPS
    mx::time_infos.frames     = 0U;
    mx::time_infos.time       = 0.0;

    // CALLBACKS
    glfwSetWindowUserPointer(ctx.window_handle, (void*)&ctx);
    glfwSetKeyCallback(ctx.window_handle, [](GLFWwindow* win, int key, int scancode, int action, int mods)
    {
        mx::context_t* ctx = (mx::context_t*)glfwGetWindowUserPointer(win);
        // for(auto keyfuns : ctx->getKeyFuns())
        //     keyfuns(key, scancode, action, mods);
        if(key == GLFW_KEY_ESCAPE)
            glfwSetWindowShouldClose(win, true);

        if(ctx->camera.controller.keypress_callback
        && action == GLFW_REPEAT)
            ctx->camera.controller.keypress_callback(&ctx->camera, key, scancode, mods);

        if(action == GLFW_PRESS)
            mx::event_manager::invoke_key_pressed(&ctx->event_manager, key, scancode, mods);
        if(action == GLFW_RELEASE)
            mx::event_manager::invoke_key_released(&ctx->event_manager, key, scancode, mods);
        if(action == GLFW_PRESS && action == GLFW_REPEAT)
            mx::event_manager::invoke_key_down(&ctx->event_manager, key, scancode, mods);
    });
    glfwSetScrollCallback(ctx.window_handle, [](GLFWwindow* win, double xscroll, double yscroll)
    {
        mx::context_t* ctx = (mx::context_t*)glfwGetWindowUserPointer(win);

        if(ctx->camera.controller.keypress_callback)
            ctx->camera.controller.mousescroll_callback(&ctx->camera, (float)xscroll, (float)yscroll);

        mx::event_manager::invoke_scroll(&ctx->event_manager, (float)xscroll, (float)yscroll);
    });
    glfwSetFramebufferSizeCallback(ctx.window_handle, [](GLFWwindow* win, int w, int h)
    {
        mx::context_t* ctx = (mx::context_t*)glfwGetWindowUserPointer(win);
        mx::context::set_size(ctx, w, h);
        #if MXRENDER_GL
            glViewport(0, 0, w, h);
        #endif //GL

        if(ctx->camera.controller.resize_callback)
            ctx->camera.controller.resize_callback(&ctx->camera, w, h);

        mx::event_manager::invoke_resize(&ctx->event_manager, w, h);

    });
    glfwSetMouseButtonCallback(ctx.window_handle, [](GLFWwindow* win, int button, int action, int mods)
    {
        mx::context_t* ctx = (mx::context_t*)glfwGetWindowUserPointer(win);

        if(action == GLFW_PRESS)
            mx::event_manager::invoke_mouse_pressed(&ctx->event_manager, button, mods);
        if(action == GLFW_RELEASE)
            mx::event_manager::invoke_mouse_released(&ctx->event_manager,button, mods);
    });

    glfwSetCursorPosCallback(ctx.window_handle, [](GLFWwindow* win, double x, double y)
    {
        mx::context_t* ctx = (mx::context_t*)glfwGetWindowUserPointer(win);

        mx::event_manager::invoke_mouse_move(&ctx->event_manager, (float)x, (float)y);
    });
    return ctx;
}

MXRDEF void mx::context::pre_render(mx::context_t* ctx)
{
    #ifdef MXRENDER_GL
        glClearColor(.1f, .1f, .1f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    #endif

    // update time
    mx::time_infos.time = glfwGetTime();
    mx::time_infos.delta_time = glfwGetTime() - ctx->last_frame_time;
    mx::time_infos.frames++;
    // printf("[delta: %f (%fs)][%d frames]: %fs since begin.\n", mx::time_infos.delta_time, ctx->last_frame_time, mx::time_infos.frames, mx::time_infos.time);
    ctx->last_frame_time = glfwGetTime();
}

MXRDEF void mx::context::set_uniforms(mx::context_t* ctx, mx::shader_t shader)
{
    mx::shader::begin(shader);
    mx::camera::set_uniforms(ctx->camera, shader);
    mx::shader::set_uniform(shader, "u_time", (float)glfwGetTime());
    mx::shader::end(shader);
}

MXRDEF void mx::context::post_render(mx::context_t* ctx)
{
    glfwSwapBuffers(ctx->window_handle);
    glfwPollEvents();
}

MXRDEF void mx::context::end()
{
    glfwTerminate();
}

MXRDEF bool mx::context::should_close(mx::context_t ctx)
{
    return glfwWindowShouldClose(ctx.window_handle);
}

MXRDEF void mx::context::set_size(mx::context_t* ctx, int w, int h)
{
    ctx->width = w;
    ctx->height = h;
    glfwSetWindowSize(ctx->window_handle, w, h);
}

////////////////////////////////////////////
//           VERTEX LAYOUT                //
////////////////////////////////////////////

MXRDEF mx::vertex_layout_t mx::vertex_layout::create(std::initializer_list<mx::vertex_layout_t::attr> attr)
{
    mx::vertex_layout_t layout;
    layout.attributes = attr;
    return layout;
}

#ifndef __str2vattrt
#define __str2vattrt(s, t) if(type == #s) return mx::vertex_layout_t::attr_type::t
static mx::vertex_layout_t::attr_type str_to_vertex_attr_type(const std::string& type)
{
    // TODO: order them by most used first
    __str2vattrt(int,    INT);
    __str2vattrt(float,  FLOAT);
    __str2vattrt(bool,   BOOL);
    __str2vattrt(float2, FLOAT2);
    __str2vattrt(uv,     FLOAT2); // spec type uv => float2
    __str2vattrt(int2,   INT2);
    __str2vattrt(float3, FLOAT3);
    __str2vattrt(rgb,    FLOAT3); // spec type rgb => float3
    __str2vattrt(int3,   INT3);
    __str2vattrt(float4, FLOAT4);
    __str2vattrt(rgba,   FLOAT3); // spec type rgba => float4
    return mx::vertex_layout_t::attr_type::FLOAT;
}
#undef __str2vattrt
#endif

MXRDEF void mx::vertex_layout::add(mx::vertex_layout_t* layout, const std::string& name, const std::string& type, bool normalize)
{
    mx::vertex_layout_t::attr_type attr_type = str_to_vertex_attr_type(type);
    layout->attributes.push_back({ name, attr_type, normalize });
}
MXRDEF void mx::vertex_layout::add(mx::vertex_layout_t* layout, const std::string& name, mx::vertex_layout_t::attr_type type, bool normalize)
{
    layout->attributes.push_back({ name, type, normalize });
}

#ifndef sattrt
    // shorten name for easy coding
    #define sattrt(t) (int)mx::vertex_layout_t::attr_type::t
#endif
// return nb of components in type
static uint32_t vattrcomp(mx::vertex_layout_t::attr attr)
{
    switch((int)attr.type)
    {
        case sattrt(BOOL):   return 1;

        case sattrt(INT):    return 1;
        case sattrt(INT2):   return 2;

        case sattrt(FLOAT):  return 1;
        case sattrt(FLOAT2): return 2;
        case sattrt(FLOAT3): return 3;
        case sattrt(FLOAT4): return 4;
        default:
            return 0;
    }
    return 0; // no corresponding return 0
}
// return bit size of type
static uint32_t vattrsize(mx::vertex_layout_t::attr attr)
{
    switch((int)attr.type)
    {
        case sattrt(BOOL): return 1;

        case sattrt(INT):  return 4;
        case sattrt(INT2): return 4 * 2;
        case sattrt(INT3): return 4 * 3;

        case sattrt(FLOAT):  return 4;
        case sattrt(FLOAT2): return 4 * 2;
        case sattrt(FLOAT3): return 4 * 3;
        case sattrt(FLOAT4): return 4 * 4;

        default:
            return 0;
    }
    return 0; // no corresponding return 0
}

MXRDEF void mx::vertex_layout::bind(uint32_t vaoID, mx::vertex_layout_t layout)
{
    #if MXRENDER_GL
    glBindVertexArray(vaoID);

    // printf("Calculating stride of %d attribute(s)\n", layout.attributes.size());
    // Calculate stride and offsets
    uint32_t stride = 0;
    for(int i = 0; i < layout.attributes.size(); i++)
    {
        layout.attributes[i].offset = stride;
        stride += vattrsize(layout.attributes[i]);
    }

    uint32_t attrib_idx = 0; // different than layout_attribute_idx because if we use matrix
    for(auto attr : layout.attributes) // loop throught the attributes
    {
        switch((int)attr.type)
        {
            case sattrt(INT):
            case sattrt(INT2):
            case sattrt(INT3):
            case sattrt(BOOL):
                // printf("glEnableVertexAttribArray(%d)\nglVertexAttribPointer(%d, %d, GL_INT, GL_FALSE, %d, %d)\n", attrib_idx, attrib_idx, vattrcomp(attr), stride, attr.offset);
                glEnableVertexAttribArray(attrib_idx);
                glVertexAttribPointer(
                    attrib_idx,
                    vattrcomp(attr),
                    GL_INT,
                    (attr.normalize) ? GL_TRUE : GL_FALSE,
                    stride,
                    (const void*)(attr.offset)
                );
                attrib_idx++;
                break;
            case sattrt(FLOAT):
            case sattrt(FLOAT2):
            case sattrt(FLOAT3):
            case sattrt(FLOAT4):
                // printf("glEnableVertexAttribArray(%d)\nglVertexAttribPointer(%d, %d, GL_FLOAT, GL_FALSE, %d, %d)\n", attrib_idx, attrib_idx, vattrcomp(attr), stride, attr.offset);
                glEnableVertexAttribArray(attrib_idx);
                glVertexAttribPointer(
                    attrib_idx,
                    vattrcomp(attr),
                    GL_FLOAT,
                    (attr.normalize) ? GL_TRUE : GL_FALSE,
                    stride,
                    (const void*)(attr.offset)
                );
                attrib_idx++;
                break;
        }
    }
    #endif
    // printf("-- Done --\n");
}
#ifdef sattrt
    #undef sattrt
#endif

////////////////////////////////////////////
//                 BATCHER                //
////////////////////////////////////////////

///////////////////////
// CREATE

template <>
mx::batch_t<mx::vertex_t> mx::batch::create(batch_params_t params)
{
    mx::batch_t<mx::vertex_t> batch;
    // set data parameters
    batch.maxShapes        = params.maxShapes;
    batch.verticesPerShape = params.verticesPerShape;
    batch.indicesPerShape  = params.indicesPerShape;
    batch.maxVertices      = params.maxShapes * params.verticesPerShape;
    batch.indicesCount     = 0;
    batch.vertexCount      = 0;

    // set data
    batch.verticesBase = new mx::vertex_t[batch.maxVertices];
    batch.verticesPtr  = &batch.verticesBase[0];

    // generate render data
    #if defined(MXRENDER_GL)
    glGenVertexArrays(1, &batch.vaoID);
    glBindVertexArray(batch.vaoID);

    glGenBuffers(1, &batch.vboID);
    glBindBuffer(GL_ARRAY_BUFFER, batch.vboID);
    glBufferData(GL_ARRAY_BUFFER, batch.maxVertices * sizeof(mx::vertex_t), nullptr, GL_DYNAMIC_DRAW);

    uint32_t* indices = new uint32_t[batch.maxShapes * batch.indicesPerShape];
    uint32_t offset = 0;
    for(uint32_t i = 0; i < batch.maxShapes * batch.vertexCount; i += batch.vertexCount)
    {
        for(uint32_t p_i = 0; p_i < batch.indicesPerShape; ++p_i)
        {
            indices[offset + p_i] = i + params.indicesTemplate[p_i];
            offset += 1;
        }
    }

    glGenBuffers(1, &batch.eboID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, batch.eboID);
    glBufferData(
        GL_ELEMENT_ARRAY_BUFFER,
        batch.maxShapes * batch.indicesPerShape * sizeof(uint32_t),
        indices,
        GL_STATIC_DRAW
    );

    if(params.vertex_layout.attributes.size() == 0)
        params.vertex_layout = mx::vertex_layout::create({
            {"pos",   mx::vertex_layout_t::attr_type::FLOAT3 }
        });
    mx::vertex_layout::bind(batch.vaoID, params.vertex_layout);
    
    glBindVertexArray(0);
    #endif // GL

    return batch;
}

template <>
mx::batch_t<mx::vertex_colored_t> mx::batch::create(batch_params_t params)
{
    mx::batch_t<mx::vertex_colored_t> batch;
    // set data parameters
    batch.maxShapes        = params.maxShapes;
    batch.verticesPerShape = params.verticesPerShape;
    batch.indicesPerShape  = params.indicesPerShape;
    batch.maxVertices      = params.maxShapes * params.verticesPerShape;
    batch.indicesCount     = 0;
    batch.vertexCount      = 0;

    // set data
    batch.verticesBase = new mx::vertex_colored_t[batch.maxVertices];
    batch.verticesPtr  = &batch.verticesBase[0];

    // generate render data
    #if defined(MXRENDER_GL)
    glGenVertexArrays(1, &batch.vaoID);
    glBindVertexArray(batch.vaoID);

    glGenBuffers(1, &batch.vboID);
    glBindBuffer(GL_ARRAY_BUFFER, batch.vboID);
    glBufferData(GL_ARRAY_BUFFER, batch.maxVertices * sizeof(mx::vertex_colored_t), nullptr, GL_DYNAMIC_DRAW);

    uint32_t* indices = new uint32_t[batch.maxShapes * batch.indicesPerShape];
    uint32_t offset = 0;
    for(uint32_t i = 0; i < batch.maxShapes * batch.vertexCount; i += batch.vertexCount)
    {
        for(uint32_t p_i = 0; p_i < batch.indicesPerShape; ++p_i)
        {
            indices[offset + p_i] = i + params.indicesTemplate[p_i];
            offset += 1;
        }
    }

    glGenBuffers(1, &batch.eboID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, batch.eboID);
    glBufferData(
        GL_ELEMENT_ARRAY_BUFFER,
        batch.maxShapes * batch.indicesPerShape * sizeof(uint32_t),
        indices,
        GL_STATIC_DRAW
    );

    if(params.vertex_layout.attributes.size() == 0)
        mx::vertex_layout_t verts_layout = mx::vertex_layout::create({
            {"pos",   mx::vertex_layout_t::attr_type::FLOAT3 },
            {"color", mx::vertex_layout_t::attr_type::FLOAT4 }
        });
    mx::vertex_layout::bind(batch.vaoID, params.vertex_layout);
    
    glBindVertexArray(0);
    #endif // GL

    return batch;
}

template <>
mx::batch_t<mx::vertex_textured_t> mx::batch::create(batch_params_t params)
{
    mx::batch_t<mx::vertex_textured_t> batch;
    // printf("Reading params...\n");
    // set data parameters
    batch.maxShapes        = params.maxShapes;
    batch.verticesPerShape = params.verticesPerShape;
    batch.indicesPerShape  = params.indicesPerShape;
    batch.maxVertices      = params.maxShapes * params.verticesPerShape;
    batch.indicesCount     = 0;
    batch.vertexCount      = 0;

    // set data
    batch.verticesBase = new mx::vertex_textured_t[batch.maxVertices];
    batch.verticesPtr  = &batch.verticesBase[0];

    // GENERATE INDICES
    uint32_t* indices = new uint32_t[batch.maxShapes * batch.indicesPerShape];
    uint32_t offset = 0;
    for(uint32_t i = 0; i < batch.maxShapes * batch.verticesPerShape; i += batch.verticesPerShape)
    {
        // printf("Idx[s.%d]: ", int(i / batch.verticesPerShape));
        for(uint32_t p_i = 0; p_i < batch.indicesPerShape; ++p_i)
        {
            indices[offset + p_i] = i + params.indicesTemplate[p_i];
            // printf("(%d:%d) ", offset + p_i, indices[offset + p_i]);
        }
        offset += batch.indicesPerShape;
        // printf("\n");
    }

    // generate render data
    // printf("Generating buffers...\n");
    #if defined(MXRENDER_GL)
    glGenVertexArrays(1, &batch.vaoID);
    glBindVertexArray(batch.vaoID);

    glGenBuffers(1, &batch.vboID);
    glBindBuffer(GL_ARRAY_BUFFER, batch.vboID);
    glBufferData(GL_ARRAY_BUFFER, batch.maxVertices * sizeof(mx::vertex_textured_t), nullptr, GL_DYNAMIC_DRAW);

    // printf("Generating buffers indices...\n");
    glGenBuffers(1, &batch.eboID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, batch.eboID);
    glBufferData(
        GL_ELEMENT_ARRAY_BUFFER,
        batch.maxShapes * batch.indicesPerShape * sizeof(uint32_t),
        indices,
        GL_STATIC_DRAW
    );

    mx::vertex_layout::bind(batch.vaoID, params.vertex_layout);

    glBindVertexArray(0);
    #endif // GL

    batch.update_buffer = true;
    return batch;
}

///////////////////////
// SET TEMPLATE

template <>
void mx::batch::set_template(mx::batch_t<mx::vertex_t>* batch, mx::vertex_t* verticesTemplate)
{
    batch->verticesTemplate = new vertex_t[batch->verticesPerShape];
    for(uint32_t i = 0; i < batch->verticesPerShape; i++)
    {
        batch->verticesTemplate[i] = verticesTemplate[i];
    }
}
template <>
void mx::batch::set_template(mx::batch_t<mx::vertex_colored_t>* batch, mx::vertex_colored_t* verticesTemplate)
{
    batch->verticesTemplate = new vertex_colored_t[batch->verticesPerShape];
    for(uint32_t i = 0; i < batch->verticesPerShape; i++)
    {
        batch->verticesTemplate[i] = verticesTemplate[i];
    }
}
template <>
void mx::batch::set_template(mx::batch_t<mx::vertex_textured_t>* batch, mx::vertex_textured_t* verticesTemplate)
{
    batch->verticesTemplate = new vertex_textured_t[batch->verticesPerShape];
    for(uint32_t i = 0; i < batch->verticesPerShape; i++)
    {
        batch->verticesTemplate[i] = verticesTemplate[i];
    }
}

///////////////////////
// ADD

MXRDEF void mx::batch::add(mx::batch_t<mx::vertex_t>* batch, vec3 pos)
{
    for(uint32_t i = 0; i < batch->verticesPerShape; ++i)
    {   
        batch->verticesPtr->pos = mx_vec3_add(pos, batch->verticesTemplate[i].pos);
        batch->verticesPtr++; // go to next emplacement in memory (<=> next idx of verticesBase)
    }
    // update counters for draw
    batch->vertexCount  += batch->verticesPerShape;
    batch->indicesCount += batch->indicesPerShape;

    batch->update_buffer = true;
}
MXRDEF void mx::batch::add(mx::batch_t<mx::vertex_colored_t>*  batch, vec3 pos, vec4 color)
{
    for(uint32_t i = 0; i < batch->verticesPerShape; ++i)
    {   
        batch->verticesPtr->pos   = mx_vec3_add(pos, batch->verticesTemplate[i].pos);
        batch->verticesPtr->color = color;
        batch->verticesPtr++; // go to next emplacement in memory (<=> next idx of verticesBase)
    }
    // update counters for draw
    batch->vertexCount  += batch->verticesPerShape;
    batch->indicesCount += batch->indicesPerShape;

    batch->update_buffer = true;
}
MXRDEF void mx::batch::add(mx::batch_t<mx::vertex_colored_t>*  batch, vec3 pos)
{
    for(uint32_t i = 0; i < batch->verticesPerShape; ++i)
    {   
        batch->verticesPtr->pos   = mx_vec3_add(pos, batch->verticesTemplate[i].pos);
        batch->verticesPtr->color = batch->verticesTemplate[i].color;
        batch->verticesPtr++; // go to next emplacement in memory (<=> next idx of verticesBase)
    }
    // update counters for draw
    batch->vertexCount  += batch->verticesPerShape;
    batch->indicesCount += batch->indicesPerShape;

    batch->update_buffer = true;
}
MXRDEF void mx::batch::add(mx::batch_t<mx::vertex_textured_t>* batch, vec3 pos, vec4 color, vec2 uv, float texID)
{
    for(uint32_t i = 0; i < batch->verticesPerShape; ++i)
    {   
        batch->verticesPtr->pos       = mx_vec3_add(pos, batch->verticesTemplate[i].pos);
        batch->verticesPtr->color     = color;
        batch->verticesPtr->texCoords = mx_vec2_add(uv, batch->verticesTemplate->texCoords);
        batch->verticesPtr->texID     = texID;
        batch->verticesPtr++; // go to next emplacement in memory (<=> next idx of verticesBase)
    }
    // update counters for draw
    batch->vertexCount  += batch->verticesPerShape;
    batch->indicesCount += batch->indicesPerShape;

    batch->update_buffer = true;
}
MXRDEF void mx::batch::add(mx::batch_t<mx::vertex_textured_t>* batch, vec3 pos, vec2 uv, float texID)
{
    for(uint32_t i = 0; i < batch->verticesPerShape; ++i)
    {   
        batch->verticesPtr->pos       = mx_vec3_add(pos, batch->verticesTemplate[i].pos);
        batch->verticesPtr->color     = batch->verticesTemplate[i].color;
        batch->verticesPtr->texCoords = mx_vec2_add(uv, batch->verticesTemplate[i].texCoords);
        batch->verticesPtr->texID     = texID;
        batch->verticesPtr++; // go to next emplacement in memory (<=> next idx of verticesBase)
    }
    // update counters for draw
    batch->vertexCount  += batch->verticesPerShape;
    batch->indicesCount += batch->indicesPerShape;

    batch->update_buffer = true;
}
MXRDEF void mx::batch::add(mx::batch_t<mx::vertex_textured_t>* batch, vec3 pos, vec4 color, float texID)
{
    for(uint32_t i = 0; i < batch->verticesPerShape; ++i)
    {   
        batch->verticesPtr->pos       = mx_vec3_add(pos, batch->verticesTemplate[i].pos);
        batch->verticesPtr->color     = color;
        batch->verticesPtr->texCoords = batch->verticesTemplate[i].texCoords;
        batch->verticesPtr->texID     = texID;
        batch->verticesPtr++; // go to next emplacement in memory (<=> next idx of verticesBase)
    }
    // update counters for draw
    batch->vertexCount  += batch->verticesPerShape;
    batch->indicesCount += batch->indicesPerShape;

    batch->update_buffer = true;
}
MXRDEF void mx::batch::add(mx::batch_t<mx::vertex_textured_t>* batch, vec3 pos, float texID)
{
    for(uint32_t i = 0; i < batch->verticesPerShape; ++i)
    {   
        batch->verticesPtr->pos       = mx_vec3_add(pos, batch->verticesTemplate[i].pos);
        batch->verticesPtr->color     = batch->verticesTemplate[i].color;
        batch->verticesPtr->texCoords = batch->verticesTemplate[i].texCoords;
        batch->verticesPtr->texID     = texID;
        batch->verticesPtr++; // go to next emplacement in memory (<=> next idx of verticesBase)
    }
    // update counters for draw
    batch->vertexCount  += batch->verticesPerShape;
    batch->indicesCount += batch->indicesPerShape;

    batch->update_buffer = true;
}

///////////////////////
// RESET BUFFER
template<>
void mx::batch::reset_buffer(mx::batch_t<mx::vertex_t>* batch)
{
    batch->indicesCount = 0; // reset indices counter
    batch->vertexCount  = 0; // reset vertices counter
    // reset pointer to begin of data
    // without needing to reset all the buffer (faster !)
    batch->verticesPtr  = &batch->verticesBase[0];
    batch->update_buffer = true;
}

template<>
void mx::batch::reset_buffer(mx::batch_t<mx::vertex_colored_t>* batch)
{
    batch->indicesCount = 0; // reset indices counter
    batch->vertexCount  = 0; // reset vertices counter
    // reset pointer to begin of data
    // without needing to reset all the buffer (faster !)
    batch->verticesPtr  = &batch->verticesBase[0];
    batch->update_buffer = true;
}

template<>
void mx::batch::reset_buffer(mx::batch_t<mx::vertex_textured_t>* batch)
{
    batch->indicesCount = 0; // reset indices counter
    batch->vertexCount  = 0; // reset vertices counter
    // reset pointer to begin of data
    // without needing to reset all the buffer (faster !)
    batch->verticesPtr  = &batch->verticesBase[0];
    batch->update_buffer = true;
}

////////////////////////
// PUSH DATA
template<>
void mx::batch::push_data(mx::batch_t<mx::vertex_t> batch)
{
    #if MXRENDER_GL
    glBindBuffer(GL_ARRAY_BUFFER, batch.vboID);
    glBufferSubData(GL_ARRAY_BUFFER, 0, batch.vertexCount * sizeof(mx::vertex_t), batch.verticesBase);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    (&batch)->update_buffer = false;
    #endif // GL
}

template<>
void mx::batch::push_data(mx::batch_t<mx::vertex_colored_t> batch)
{
    #if MXRENDER_GL
    glBindBuffer(GL_ARRAY_BUFFER, batch.vboID);
    glBufferSubData(GL_ARRAY_BUFFER, 0, batch.vertexCount * sizeof(mx::vertex_colored_t), batch.verticesBase);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    (&batch)->update_buffer = false;
    #endif // GL
}

template<>
void mx::batch::push_data(mx::batch_t<mx::vertex_textured_t> batch)
{
    #if MXRENDER_GL
    glBindBuffer(GL_ARRAY_BUFFER, batch.vboID);
    glBufferSubData(GL_ARRAY_BUFFER, 0, batch.vertexCount * sizeof(mx::vertex_textured_t), batch.verticesBase);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    (&batch)->update_buffer = false;
    #endif // GL
}

///////////////////////
// FLUSH

template <>
void mx::batch::flush(mx::batch_t<mx::vertex_t> batch, bool draw_indexed, bool reset_buffer)
{
    #if MXRENDER_GL
    if(batch.update_buffer) // prevent sending data again and again to the gpu
    {
        glBindVertexArray(batch.vaoID);
        glBufferSubData(GL_ARRAY_BUFFER, 0, batch.vertexCount * sizeof(mx::vertex_t), batch.verticesBase); // push data to buffer
        (&batch)->update_buffer = false;
    }
    if(draw_indexed)
    {
        // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, batch.eboID);
        glDrawElements(GL_TRIANGLES, batch.indicesCount, GL_UNSIGNED_INT, nullptr);
    }
    else
        glDrawArrays(GL_TRIANGLES, 0, batch.vertexCount); // draw triangles raw
    glBindVertexArray(0);
    #endif //GL

    if(reset_buffer)
    {
        mx::batch::reset_buffer<mx::vertex_t>(&batch);
    }
}

template <>
void mx::batch::flush(mx::batch_t<mx::vertex_colored_t> batch, bool draw_indexed, bool reset_buffer)
{
    #if MXRENDER_GL
    if(batch.update_buffer) // prevent sending data again and again to the gpu
    {
        glBindVertexArray(batch.vaoID);
        glBufferSubData(GL_ARRAY_BUFFER, 0, batch.vertexCount  * sizeof(mx::vertex_colored_t), batch.verticesBase); // push data to buffer
        (&batch)->update_buffer = false;
    }
    if(draw_indexed)
        glDrawElements(GL_TRIANGLES, batch.indicesCount, GL_UNSIGNED_INT, nullptr);
    else
        glDrawArrays(GL_TRIANGLES, 0, batch.vertexCount); // draw triangles raw
    glBindVertexArray(0);
    #endif //GL

    if(reset_buffer)
    {
        mx::batch::reset_buffer<mx::vertex_colored_t>(&batch);
    }
}

template <>
void mx::batch::flush(mx::batch_t<mx::vertex_textured_t> batch, bool draw_indexed, bool reset_buffer)
{
    #if MXRENDER_GL
    if(batch.update_buffer)
    {
        glBindVertexArray(batch.vaoID);
        glBufferSubData(GL_ARRAY_BUFFER, 0, batch.vertexCount * sizeof(mx::vertex_textured_t), batch.verticesBase); // push data to buffer
        (&batch)->update_buffer = false;
    }
    if(draw_indexed)
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, batch.eboID);
        glDrawElements(GL_TRIANGLES, batch.indicesCount, GL_UNSIGNED_INT, nullptr);
    }
    else
        glDrawArrays(GL_TRIANGLES, 0, batch.vertexCount); // draw triangles raw
    glBindVertexArray(0);
    #endif //GL

    if(reset_buffer)
    {
        mx::batch::reset_buffer<mx::vertex_textured_t>(&batch);
    }
}

////////////////////////////////////////////
//                 TILES                  //
////////////////////////////////////////////

bool mx::tile_t::operator== (const mx::tile_t& o) 
{
    return o.id == this->id;
}

////////////////////////////////////////////
//                TILESET                 //
////////////////////////////////////////////

MXRDEF mx::tileset_t mx::tileset::load_tileset(const ::std::string& atlas)
{
    mx::tileset_t t;
    t.atlas = mx::texture::load_from_file(atlas.c_str());
    t.count = 0;
    t.tiles = { };
    return t;
}

MXRDEF void mx::tileset::auto_tiles(mx::tileset_t* tileset, vec2 tile_count)
{
    tileset->count = (int)tile_count.x * (int)tile_count.y;
    tileset->tiles = { };
    vec2 uv_per_tile = { 1.f / tile_count.x, 1.f / tile_count.y };
    for(int y = 0; y < tile_count.y; ++y)
    {
        for(int x = 0; x < tile_count.x; ++x)
        {
            int i = x + y * (int)tile_count.x;
            mx::tile_t tile;
            tile.id = i;
            tile.texpos  = { x * uv_per_tile.x, y * uv_per_tile.y };
            tile.texsize = { uv_per_tile };
            tileset->tiles.insert({ (mx::tiletype)i, tile });
        }
    }
}

MXRDEF void mx::tileset::add_tile(mx::tileset_t* tileset, vec2 pixelorigin, vec2 pixelsize, mx::tiletype type)
{
    mx::tile_t tile;
    tile.id = tileset->count;
    tile.texpos  = { pixelorigin.x / (float)tileset->atlas.width, pixelorigin.y / (float)tileset->atlas.height };
    tile.texsize = { pixelsize.x   / (float)tileset->atlas.width, pixelsize.y   / (float)tileset->atlas.height };
    tileset->tiles.insert({ type, tile });
    tileset->count += 1;
}

////////////////////////////////////////////
//                TILEMAP                 //
////////////////////////////////////////////

MXRDEF mx::tiletype mx::autotiling::test_4(mx::tilemap_t tilemap, vec2 tilepos, int tile_value)
{
    // not a valid cell, return 0
    if(!mx::tilemap::is_valid_cell(tilemap, (uint32_t)tilepos.x, (uint32_t)tilepos.y)) return mx::tiletype::NONE;
    if(tilemap.data[(int)tilepos.x][(int)tilepos.y] != tile_value) return mx::tiletype::NONE;

    std::bitset<4> n(0);
    vec2 dirs[] = {
        { 0, -1}, // bottom
        { 1,  0}, // right
        {-1,  0}, // left
        { 0,  1}, // top
    };
    for(int n_i = 0; n_i < 4; ++n_i) {
        vec2 dir = dirs[n_i];
        int xx = tilepos.x + (int)dir.x;
        int yy = tilepos.y + (int)dir.y;
        if(!mx::tilemap::is_valid_cell(tilemap, xx, yy))
        {
            n.set(n_i, true);
            continue;
        }

        n.set(n_i, (tilemap.data[xx][yy] != tile_value));
    }
    int bitsetval =
          1 * n.test(0) 
        + 2 * n.test(1) 
        + 4 * n.test(2) 
        + 8 * n.test(3);
    
    return (mx::tiletype)bitsetval;
}


MXRDEF mx::tilemap_t mx::tilemap::create(int width, int height, mx::tileset_t tileset, vec2 tile_pixelsize, int default_value)
{
    printf("Generating tilemap...\n");
    // PARAMS
    mx::tilemap_t tilemap;
    tilemap.height = height;
    tilemap.width  = width;
    tilemap.tileset = { tileset };
    int tile_count = tileset.count ? tileset.count : 1;
    tilemap.tile_pixelsize = { tile_pixelsize };
    tilemap.pos = { (float)width * -.5f, (float)height * -.5f, .0f};

    // BATCH TILES
    uint32_t quad_indices[] = { 0, 1, 2, 2, 3, 0 };
    mx::batch_params_t quad_batch_params = {
        (uint32_t)width * (uint32_t)height,
        4,
        6,
        quad_indices,
        mx::vertex_layout::create({
            { "pos",       mx::vertex_layout_t::attr_type::FLOAT3 },
            { "color",     mx::vertex_layout_t::attr_type::FLOAT4 },
            { "texCoords", mx::vertex_layout_t::attr_type::FLOAT2 },
            { "texID",     mx::vertex_layout_t::attr_type::FLOAT  }
        })
    };
    tilemap.tilebatch = mx::batch::create<mx::vertex_textured_t>(quad_batch_params);
    vec2 uv_per_tile = { 
        tilemap.tile_pixelsize.x / (float)tileset.atlas.width, 
        tilemap.tile_pixelsize.y / (float)tileset.atlas.height 
    };
    mx::vertex_textured_t quad_verts_template[4] = {
        // pos                  // color                 // uv                       // texID
        {{ -0.5f, -0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, {           0.f, uv_per_tile.y }, .0f}, // top left
        {{  0.5f, -0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { uv_per_tile.x, uv_per_tile.y }, .0f}, // top right
        {{  0.5f,  0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, { uv_per_tile.x,           0.f }, .0f}, // bottom right
        {{ -0.5f,  0.5f, .0f }, { 1.f, 1.f, 1.f, 1.f }, {           0.f,           0.f }, .0f}, // bottom left
    };
    mx::batch::set_template(&tilemap.tilebatch, quad_verts_template);

    // GENERATING MAP AND FIRST QUAD
    mx_vec4 color = {1.f, 1.f, 1.f, 1.f};
    tilemap.data = new int*[width];
    for(uint32_t x = 0; x < width; ++x)
    {
        tilemap.data[x] = new int[height];
        for(uint32_t y = 0; y < height; ++y)
        {
            tilemap.data[x][y] =  tilemap.default_value;
            mx::batch::add(&tilemap.tilebatch, {float(x), float(y), .0f}, color, (float)tilemap.data[x][y]);
        }
    }
    printf("-- DONE\n");

    return tilemap;
}

MXRDEF void mx::tilemap::generate_geometry(mx::tilemap_t tilemap)
{
    if(tilemap.has_changed)
    {
        mx::batch::reset_buffer(&tilemap.tilebatch);
        for(uint32_t x = 0; x < tilemap.width; ++x)
            for(uint32_t y = 0; y < tilemap.height; ++y)
            {
                vec2 computedUV;
                int tile_idx    = tilemap.data[x][y];
                mx::tile_t tile = tilemap.tileset.tiles[(mx::tiletype)tile_idx];
                computedUV      = { tile.texpos };
                mx::batch::add(&tilemap.tilebatch, {float(x), float(y), .0f}, computedUV);
            }
    }
}

MXRDEF void mx::tilemap::generate_geometry_with_autotiling(mx::tilemap_t tilemap)
{
    if(tilemap.has_changed)
    {
        mx::batch::reset_buffer(&tilemap.tilebatch);
        for(int x = 0; x < tilemap.width; ++x) {
            for(int y = 0; y < tilemap.height; ++y)
            {

                mx::tiletype type = mx::autotiling::test_4(tilemap, { (float)x, (float)y }, tilemap.tileset.map_value);
                if(tilemap.tileset.tiles.find(type) == tilemap.tileset.tiles.end())
                    type = mx::tiletype::NONE;
                mx::tile_t tile = tilemap.tileset.tiles[type];

                // apply tile found
                mx::batch::add(&tilemap.tilebatch, {float(x), float(y), .0f}, tile.texpos);
            }
        }
    }
}


MXRDEF bool mx::tilemap::is_valid_cell(mx::tilemap_t tilemap, uint32_t x, uint32_t y)
{
    return (0 <= x && x < (int)tilemap.width)
        && (0 <= y && y < (int)tilemap.height);
}

MXRDEF void mx::tilemap::set_tile(mx::tilemap_t* tilemap, uint32_t x, uint32_t y, int value)
{
    if(!is_valid_cell(*tilemap, x, y)) return;
    tilemap->data[x][y]  = value;
    tilemap->has_changed = true;
}

MXRDEF void mx::tilemap::draw(mx::tilemap_t tilemap, mx::shader_t shader)
{
    // Model matrix
    mx_mat4 model = mx_mat4_translate(mx_mat4_identity(), tilemap.pos);
    mx::shader::begin(shader);

    mx::texture::bind(tilemap.tileset.atlas, 1);
    mx::shader::set_uniform(shader, "tile_atlas", 1);
    mx::shader::set_uniform(shader, "u_tilingFactor", 1.f); // set to 1

    mx::shader::set_uniform_mat(shader, "u_model", mx_mat4_raw(model).ptr, 4);

    mx::batch::flush(tilemap.tilebatch, true, tilemap.has_changed);
    
    mx::shader::end(shader);
    tilemap.has_changed = false;
}