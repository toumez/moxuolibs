#ifndef MX_INCLUDE_MXRENDER_H
#define MX_INCLUDE_MXRENDER_H

#define MXRENDER_VERSION 1

// RENDER APIS
#if defined(MXRENDER_USE_VULKAN)
    #error "mxrender doesn't support Vulkan yet."
#elif defined(MXRENDER_USE_METAL)
    #error "mxrender doesn't support Metal yet."
#elif defined(MXRENDER_USE_DX)
    #error "mxrender doesn't support DX yet."
#else // Default to GL
    #define MXRENDER_GL 1
    #if !defined(MXRENDER_USE_GL) && !defined(MXRENDER_USE_GLES)
        #define MXRENDER_USE_GL 1
    #endif
    #ifndef __glad_h_
        #include <glad/glad.h>
    #endif
#endif


#ifndef _glfw3_h_
    #include <GLFW/glfw3.h>
    #ifndef _glfw3_h_
        #error "This library require glfw3"
    #endif
#endif

#ifndef STBI_VERSION
    #include <stb/stb_image.h>
    #ifndef STBI_VERSION
        #error "This library require stbimage"
    #endif
#endif

#ifndef __glm_h__
    #define MXM_INCLUDE_IMPLEMENTATION
    #include <mx/mxmaths.h>
    #ifndef __mxmaths_h__
        #error "Require glm or mxmaths"
    #endif
#endif

#ifndef __cplusplus
    #error "This is a c++ lib"
#endif


//////////////////////////////////////////////////
// HEADER FILE
#define MXRDEF extern
// #if !defined(MXR_STATIC)
//     #define MXRDEF static
// #else
//    #define MXRDEF extern
// #endif

#include <vector>
#include <map>
#include <string>
#ifndef mat4
    #ifdef __mxmaths_h__
        using mat4 = mx_mat4;
    #elif defined(__glm_h__)
        using mat4 = glm::mat4;
    #endif
#endif

#ifndef vec2
    #ifdef __mxmaths_h__
        using vec2 = mx_vec2;
    #elif defined(__glm_h__)
        using vec2 = glm::vec2;
    #endif
#endif

#ifndef vec3
    #ifdef __mxmaths_h__
        using vec3 = mx_vec3;
    #elif defined(__glm_h__)
        using vec3 = glm::vec3;
    #endif
#endif

#ifndef vec4
    #ifdef __mxmaths_h__
        using vec4 = mx_vec4;
    #elif defined(__glm_h__)
        using vec4 = glm::vec4;
    #endif
#endif

namespace mx {

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    // time infos of the current app
    struct time_infos_t
    {
        double delta_time;
        double time;
        uint64_t frames;
    };
    static time_infos_t time_infos;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    MXRDEF std::string __mxr_cwd();
    MXRDEF std::string __mxr_absolute_path(std::string p);
    MXRDEF std::string ___read_file(std::string path);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // texture
    // represented by a render api id
    // render api binding idx
    // texture size
    typedef struct
    {
        uint32_t rendererID = 0;
        uint32_t binding_index = 0;
        int width = 1, height = 1;
    } texture_t;

    // sub texture from a texture
    // represented by uv pos in a texture
    typedef struct
    {
        texture_t texture;
        vec2 pos;
        vec2 size;
    } subtexture_t;
    namespace texture {
        // create texture from pixel data
        MXRDEF texture_t create(unsigned char* data, int width, int height, int channels);
        // create texture from a image file using stbi
        MXRDEF texture_t load_from_file(const char* path);

        // create sub texture from pixel pos and pixel size
        MXRDEF subtexture_t create_subtexture(texture_t tex, vec2 pos, vec2 size);

        // bind texture to render api
        MXRDEF void bind(texture_t t, int index = 0);
        // unbind texture to render api
        MXRDEF void unbind(texture_t);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // shader
    typedef struct
    {
        uint32_t rendererID;
        std::string name;
    } shader_t;
    namespace shader {
        // create shader from vertex code and fragment code
        MXRDEF shader_t create(const char* vcode, const char* fcode);
        // create shader from vertex glsl file and fragment glsl file
        MXRDEF shader_t load_from_files(std::string vpath, std::string fpath);
        // load a render api shader from code and shader type
        MXRDEF uint32_t load_shader(std::string code, int type);

        // begin draw using shader
        MXRDEF void begin(shader_t s);
        // end draw using shader
        MXRDEF void end(shader_t s);

        // set an uniform (or varying) in a shader
        // [use mx::shader::begin before use]
        template<typename T>
        MXRDEF void set_uniform(shader_t shader, std::string name, T value);

        // set an uniform (or varying) array in a shader
        // [use mx::shader::begin before use]
        template <typename T>
        MXRDEF void set_uniform_array(shader_t shader, std::string name, T* values, uint32_t count);

        // Set a square matrix uniform (or varying) to a shader program
        // [use mx::shader::begin before use]
        MXRDEF void set_uniform_mat(shader_t shader, std::string name, float* valueptr, int size);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Simple events
    namespace events
    {
        // #define REGISTER_EVENT(name, ...) typedef void (*name)(...);
        #define nullevent nullptr;

        // CORE EVENTS
        
        // typedef void (*start) (int argc, char* argv[]);
        // typedef void (*update)(float delta);
        // typedef void (*render)(float delta);
        // typedef void (*end)   ();

        // ENVIRONNEMENT EVENTS
        
        // event occuring when the window (or context) is resized
        typedef void (*resize)        (void* obj, int w, int h);


        // INPUT EVENTS

        // event occuring the first frame when a key is pressed
        typedef void (*key_pressed)    (void* obj, int key, int scancode, int mods);
        // event occuring when a key is hold down (and pressed)
        typedef void (*key_down)       (void* obj, int key, int scancode, int mods);
        // event occuring when a key is released
        typedef void (*key_released)   (void* obj, int key, int scancode, int mods);

        // event occuring the first frame a mouse button is pressed
        typedef void (*mouse_pressed)  (void* obj, int button, int mods);
        // event occuring when a mouse button is released
        typedef void (*mouse_released) (void* obj, int button, int mods);
        // event occuring when the mouse move
        typedef void (*mouse_move)     (void* obj, float x, float y);
        // event occuring when the mouse wheel is used
        typedef void (*scroll)         (void* obj, float x_off, float y_off);
    }
    // Simple event manager
    typedef struct
    {
        // What describe a event in mx
        struct __event_descriptor_t
        {
            void* event_fnc;
            void* obj;
            // void* params;
        };

        // events registered in the manager
        std::map<std::string, std::vector<__event_descriptor_t>> registered_events { };
    } event_manager_t;
    namespace event_manager 
    {
        // register an event into a manager by event name and event function (see mx::events)
        MXRDEF void register_on(event_manager_t* manager, std::string event_name, void* eventfnc);

        #ifndef REGISTER_EVT_FNC
        #define REGISTER_EVT_FNC(name, ...)\
            MXRDEF void register_on_##name(event_manager_t* manager, void* obj, events::name fnc);\
            MXRDEF void invoke_##name     (event_manager_t* manager, __VA_ARGS__)
        #endif

        REGISTER_EVT_FNC(resize, int width, int height);

        REGISTER_EVT_FNC(key_pressed , int key, int scancode, int mods);
        REGISTER_EVT_FNC(key_down    , int key, int scancode, int mods);
        REGISTER_EVT_FNC(key_released, int key, int scancode, int mods);

        REGISTER_EVT_FNC(scroll,         float xoff, float yoff);
        REGISTER_EVT_FNC(mouse_pressed,  int button, int mods);
        REGISTER_EVT_FNC(mouse_released, int button, int mods);
        REGISTER_EVT_FNC(mouse_move,     float xpos, float ypos);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Default controller for camera
    typedef struct
    {
        float min_zoom   = 1.f;
        float max_zoom   = 1000.f;
        float zoom_speed = 250.f;
        float move_speed = 300.f;

        events::resize        resize_callback      = nullevent;
        events::key_pressed   keypress_callback    = nullevent;
        events::mouse_move    mousemove_callback   = nullevent;
        events::mouse_pressed mousepress_callback  = nullevent;
        events::scroll        mousescroll_callback = nullevent;
    } camera_controller_t;

    // camera
    typedef struct
    {
        // projection matrix (to make points fit into camera plane)
        mat4  proj;
        // view matrix (camera pos, rot, scale)
        mat4  view;
        vec3  pos;

        float zoom;
        vec2  size;

        camera_controller_t controller;
    } camera_t;

    namespace camera
    {
        // create a camera
        MXRDEF camera_t create(vec3 pos = {.0f, 0.0f, 0.f}, vec2 size = { 800.f, 600.f }, float zoom = 1.f);

        // move the camera by a certain amount
        MXRDEF void move(camera_t* cam, vec3 pos);
        // void lookat(vec3 target);
        MXRDEF void set_uniforms(camera_t cam, shader_t shader);
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // graphical context
    typedef struct
    {
        GLFWwindow* window_handle = nullptr;
        int width = 1, height = 1;
        std::string name = "mxctx";

        camera_t camera = {};
        bool can_camera_move = false;

        // time
        double last_frame_time = .0;

        // events
        event_manager_t event_manager;
    } context_t;
    namespace context {
        // create graphical context (window + gl/vulkan/...)
        // width / height / window name
        MXRDEF context_t create(int w, int h, std::string name, bool can_camera_move = false);
        
        // end context with all the process link into it
        MXRDEF void end();
        
        // call before rendering anything
        MXRDEF void pre_render (context_t* ctx);
        // Set default uniform for a context (u_proj, u_view and u_time)
        MXRDEF void set_uniforms(context_t* ctx, mx::shader_t shader);
        // call after rendering everything (swap buffers & poll events)
        MXRDEF void post_render(context_t* ctx);

        // set size of window + viewport
        MXRDEF void set_size(context_t* ctx, int w, int h);

        // return if window should close
        MXRDEF bool should_close(context_t ctx);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // vao layout (array of attr)
    struct vertex_layout_t
    {
        // vertex attr type
        enum class attr_type : int {
            INT = 0,   INT2,   INT3,
            FLOAT,   FLOAT2, FLOAT3, FLOAT4,
            BOOL
        };

        // vertex attr infos
        typedef struct 
        {
            std::string name;
            attr_type type;
            bool normalize = false;
            uint32_t offset = 0;
        } attr;

        std::vector<attr> attributes;
    };
    // layout of the vertex in buffer
    namespace vertex_layout
    {
        // create a layout
        MXRDEF vertex_layout_t create(std::initializer_list<vertex_layout_t::attr> attr = {});

        // add an attribute with type as string (check the definition to know)
        MXRDEF void add(vertex_layout_t* layout, const std::string& name, const std::string& type, bool normalize = false);
        // add an attribute
        MXRDEF void add(vertex_layout_t* layout, const std::string& name, vertex_layout_t::attr_type type, bool normalize = false);

        // bind attributes to a vao
        MXRDEF void bind(uint32_t vaoID, vertex_layout_t layout);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // simple vertex
    typedef struct 
    {
        vec3  pos       = {.0f, .0f, .0f};      // origin to 0   (default)
    } vertex_t;
    // vertex with color
    typedef struct
    {
        vec3  pos       = {.0f, .0f, .0f};      // origin to 0   (default)
        vec4  color     = {1.f, 1.f, 1.f, 1.f}; // white color   (default)
    } vertex_colored_t;
    // vertex with color and texCoords and texID (for multiple texture)
    typedef struct
    {
        vec3  pos       = {.0f, .0f, .0f};      // origin to 0   (default)
        vec4  color     = {1.f, 1.f, 1.f, 1.f}; // white color   (default)
        vec2  texCoords = {.0f, .0f };          // bottom left   (default)
        float texID     = 0.f;                  // white texture (default)
    } vertex_textured_t;

    // batch is a struct to store batch process
    template <typename vertexT>
    struct batch_t
    {
        uint32_t verticesPerShape = 4;
        vertexT* verticesTemplate = nullptr;
        
        uint32_t maxShapes = 1024;
        uint32_t maxVertices = maxShapes * verticesPerShape;
        
        uint32_t  indicesPerShape = 6;

        vertexT* verticesBase = nullptr;
        vertexT* verticesPtr  = nullptr;

        uint32_t vertexCount  = 0;
        uint32_t indicesCount = 0;

        bool update_buffer = true;

        // rendererIDs
        uint32_t vaoID = 0;
        uint32_t vboID = 0;
        uint32_t eboID = 0;
    };

    // batch template declaration to be compiled
    #ifndef REGISTER_BATCH_T
    #define REGISTER_BATCH_T(vT) template struct batch_t<vT>
    REGISTER_BATCH_T(vertex_t);
    REGISTER_BATCH_T(vertex_colored_t);
    REGISTER_BATCH_T(vertex_textured_t);
    #undef REGISTER_BATCH_T
    #endif

    // BATCH PARAMS
    struct batch_params_t
    {
        // maxShapes (nb of shape in batch)
        uint32_t  maxShapes        = 1024;
        // verticesPerShape
        uint32_t  verticesPerShape = 4;
        // indicesPerShape (must be equal to indicesTemplate count)
        uint32_t  indicesPerShape  = 6;
        // indices template (array of indices)
        uint32_t* indicesTemplate  = nullptr;
        // vertex_layout (how the attribute of the vertex are set see: mx::vertex_layout funcs)
        vertex_layout_t vertex_layout;
    };
    namespace batch {
        // Require some params and a vertexType (vertexT)
        // which require at least a vec3 called pos in member
        template <typename T>
        MXRDEF batch_t<T> create(batch_params_t params);
        
        // set a template for adding a shape
        template <typename T>
        MXRDEF void set_template(batch_t<T>* batch, T* verticesTemplate);

        // Add a vertex_t to batch
        MXRDEF void add(batch_t<mx::vertex_t>*          batch, vec3 pos);
        // Add a vertex_colored_t to batch
        MXRDEF void add(batch_t<mx::vertex_colored_t>*  batch, vec3 pos, vec4 color = { 1.f, 1.f, 1.f, 1.f});
        // Add a vertex_colored_t to batch
        MXRDEF void add(batch_t<mx::vertex_colored_t>*  batch, vec3 pos);
        // Add a vertex_textured_t to batch
        MXRDEF void add(batch_t<mx::vertex_textured_t>* batch, vec3 pos, vec4 color, vec2 uv, float texID = .0f);
        // Add a vertex_textured_t to batch
        MXRDEF void add(batch_t<mx::vertex_textured_t>* batch, vec3 pos, vec2 uv, float texID = .0f);
        // Add a vertex_textured_t to batch
        MXRDEF void add(batch_t<mx::vertex_textured_t>* batch, vec3 pos, vec4 color, float texID = .0f);
        // Add a vertex_textured_t to batch
        MXRDEF void add(batch_t<mx::vertex_textured_t>* batch, vec3 pos, float texID = .0f);

        // Reset the vbo of a batch
        template<typename T>
        MXRDEF void reset_buffer(batch_t<T>* batch);

         // Update the data to the vbo
        template<typename T>
        MXRDEF void push_data(batch_t<T> batch);

        // Flush data to screen
        // Draw all the buffer into screen
        template <typename T>
        MXRDEF void flush(batch_t<T> batch, bool draw_indexed = true, bool reset_buffer = false);

        // batch template funcs declaration to be compiled
        #ifndef REGISTER_BATCH_FUNCS_T
        #define REGISTER_BATCH_FUNCS_T(vT)                                                \
            template<> batch_t<vT> create(batch_params_t params);                         \
            template<> void set_template(batch_t<vT>* batch, vT* verticesTemplate);       \
            template<> void reset_buffer(batch_t<vT>* batch);                             \
            template<> void push_data(batch_t<vT> batch);                                  \
            template<> void flush(batch_t<vT> batch, bool draw_indexed, bool reset_buffer);
        #endif

        REGISTER_BATCH_FUNCS_T(vertex_t);
        REGISTER_BATCH_FUNCS_T(vertex_colored_t);
        REGISTER_BATCH_FUNCS_T(vertex_textured_t);

        #ifdef REGISTER_BATCH_FUNCS_T
            #undef REGISTER_BATCH_FUNCS_T
        #endif
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // SOURCE: [https://gamedevelopment.tutsplus.com/tutorials/how-to-use-tile-bitmasking-to-auto-tile-your-level-layouts--cms-25673]
    // tile rule
    enum class tiletype : int
    {
        // 4 bit (15 cases)
        // BRLT (Bottom Right Left Top)

        CENTER      = 0,
        BOTTOM     ,
        RIGHT      ,
        BR_CORNER  ,
        LEFT       ,
        BL_CORNER  ,
        COL_MIDDLE ,
        U_BOTTOM   ,
        TOP        ,
        ROW_MIDDLE ,
        TR_CORNER  ,
        U_RIGHT    ,
        TL_CORNER  ,
        U_LEFT     ,
        U_TOP      ,
        NONE

        // 8 bits values
        // { 2 = 1, 8 = 2, 10 = 3, 11 = 4, 16 = 5, 18 = 6, 22 = 7, 24 = 8, 26 = 9, 27 = 10, 30 = 11, 31 = 12, 64 = 13, 66 = 14, 72 = 15, 74 = 16, 75 = 17, 80 = 18, 82 = 19, 86 = 20, 88 = 21, 90 = 22, 91 = 23, 94 = 24, 95 = 25, 104 = 26, 106 = 27, 107 = 28, 120 = 29, 122 = 30, 123 = 31, 126 = 32, 127 = 33, 208 = 34, 210 = 35, 214 = 36, 216 = 37, 218 = 38, 219 = 39, 222 = 40, 223 = 41, 248 = 42, 250 = 43, 251 = 44, 254 = 45, 255 = 46, 0 = 47 }
    };

    // tile infos
    struct tile_t {
        int id;
        vec2 texpos;
        vec2 texsize;
        bool operator== (const tile_t& o);
    };
    
    // tile atlas infos
    struct tileset_t
    {
        std::map<tiletype, tile_t> tiles { };
        int count;
        int map_value = 1;
        mx::texture_t atlas;
    };
    namespace tileset
    {
        // load tileset
        // MXRDEF tileset_t load_tileset_json(const ::std::string& tileset_path);
        MXRDEF tileset_t load_tileset(const ::std::string& atlas);

        // auto convert a atlas into tiles
        MXRDEF void auto_tiles(tileset_t* tileset, vec2 tile_count);
        // add a tile into the tileset
        MXRDEF void add_tile(tileset_t* tileset, vec2 pixelorigin, vec2 pixelsize, tiletype type);
    }

    // represent the info of a cell in a tilemap
    typedef struct
    {
        int base_value;
        int tile_id;
        int tileset_id;
    } tilemap_cell_t;

    // tilemap
    struct tilemap_t
    {
        int width;
        int height;
        int** data; // tab

        tileset_t tileset;
        vec2 tile_pixelsize;

        int default_value = 0;
        bool has_changed = false;

        mx::batch_t<mx::vertex_textured_t> tilebatch;

        vec3 pos;
    };
    namespace autotiling {
        // test the tile in north / west / east / south and return the sum of the bit values of the corresponding directions
        // into a tiletype which correspond to his position and context
        MXRDEF tiletype test_4(tilemap_t tilemap, vec2 tilepos, int tile_value);
        // MXRDEF int test_8(tilemap_t tilemap, tiletype tiletype, vec2 tilepos, int empty_tile_value);

        // MXRDEF tiletype test_4_multiple_terrains(tilemap_t tilemap, tiletype tiletype, vec2 tilepos, int empty_tile_value);
        // MXRDEF int test_8_multiple_terrains(tilemap_t tilemap, tiletype tiletype, vec2 tilepos, int empty_tile_value);
    }
    namespace tilemap
    {
        // create a tilemap
        MXRDEF tilemap_t create(int width, int height, mx::tileset_t tileset, vec2 tile_pixelsize = { 16.f, 16.f }, int default_value = 0);

        // generate the batch data for the tiles
        MXRDEF void generate_geometry(tilemap_t tilemap);

        // generate the batch data for the tiles with some rules
        MXRDEF void generate_geometry_with_autotiling(tilemap_t tilemap);

        // Return if the coord is in the tilemap bounds
        MXRDEF bool is_valid_cell(tilemap_t tilemap, uint32_t x, uint32_t y);

        // Set the value of a certain tile (if in bounds)
        MXRDEF void set_tile(tilemap_t* tilemap, uint32_t x, uint32_t y, int value);

        // Draw the (whole) tilemap to the screen
        MXRDEF void draw(tilemap_t tilemap, mx::shader_t shader);
    }
}
#endif //MX_INCLUDE_MXRENDER_H


