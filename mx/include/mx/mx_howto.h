#ifndef MX_INCLUDE_MXHOW_H
#define MX_INCLUDE_MXHOW_H

#define MXHOW_VERSION 1

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////
// HEADER FILE
#if !defined(MXH_STATIC)
    #define MXHDEF static
#else
    #define MXHDEF extern
#endif


/*
...
Declarations of func and struct here

Ex:
typedef struct
{
    int level;
    const char* content
} mx_msg;
MXHDEF void say(mx_msg msg);
*/

#ifdef __cplusplus
}
#endif
// END HEADER FILE
//////////////////////////////////////////////////

#ifdef MXHOW_INCLUDE_IMPLEMENTATION
// DEBUG ASSERT
#ifndef MXH_ASSERT
    #include <assert.h>
    #define MXH_ASSERT(x) assert(x)
#endif //MXM_ASSERT

// EXTERN TYPE
#ifdef __cplusplus
    #define MXH_EXTERN extern "C"
#else
    #define MXH_EXTERN extern
#endif //__cplusplus

// INLINE TYPE
#ifdef _MSC_VER
    #ifdef __cplusplus
        #define mxh_inline inline
    #else
        #define mxh_inline
    #endif //__cplusplus
#else
    #define mxh_inline __forceinline
#endif //_MSC_VER


// Define implementations here
/*

Ex:

#include <iostream>
MXHDEF void say(mx_msg msg)
{
    std::cout << "[" << msg.level << "]: " << msg.content << "\n";
    MXC_ASSERT(msg.level < 4); // 5 is critical
}
*/


#endif //MXCORE_INCLUDE_IMPLEMENTATION
#endif //MX_INCLUDE_MXCORE_H